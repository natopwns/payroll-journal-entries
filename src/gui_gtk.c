// gui_gtk.c - Loading the pyed glade file.

#include "gui_gtk.h"

// Functions--------------------------------------------------------------------
int load_from_glade(GtkWidget ** window, GtkBuilder ** builder)
{
	*builder = gtk_builder_new();

	int result = gtk_builder_add_from_file(*builder, "../glade/main_window.glade", NULL);
	if (result == 0)
	{
		printf("gtk_builder_add_from_file FAILED\n");
		return 1;
	}

	*window = GTK_WIDGET(gtk_builder_get_object(*builder, "MainWindow"));

	return 0;
}

void connect_signals(GtkBuilder * builder, GtkWidget * window)
{
	// get widgets from builder
	GtkWidget * organize_button = GTK_WIDGET(gtk_builder_get_object(builder, "OrganizeButton"));

	// connect signals from widgets to functions
	g_signal_connect(window, "delete_event", G_CALLBACK(window_delete), NULL);
	g_signal_connect(organize_button, "clicked", G_CALLBACK(button_pressed), NULL);
}
// Functions--------------------------------------------------------------------


// Callbacks--------------------------------------------------------------------
gboolean window_delete(GtkWidget * widget, GdkEvent * event, gpointer data)
{
	(void)widget;
	(void)event;
	(void)data;

	g_print("%s called.\n", __FUNCTION__);
	gtk_main_quit();

	return FALSE;
}

void button_pressed(GtkWidget * widget, GdkEvent * event, gpointer data)
{
	(void)widget;
	(void)event;
	(void)data;

	g_print("You pressed a button. Good job.\n");
}
// Callbacks--------------------------------------------------------------------

