// pyed-payroll.c - main file.

#include "gui_gtk.h"

// Execution--------------------------------------------------------------------
int main(int argc, char ** argv)
{
	GtkWidget * window;
	GtkBuilder * builder = NULL;

	gtk_init(&argc, &argv);

	int result = load_from_glade(&window, &builder);
	if (result == 1)
		return 1;
	
	connect_signals(builder, window);

	gtk_widget_show_all(window);
	gtk_main();

	return 0;
}
// Execution--------------------------------------------------------------------
