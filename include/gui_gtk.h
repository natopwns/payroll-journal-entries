// gui_gtk.h - declarations for gui_gtk.c

#ifndef GUI_GTK_H
#define GUI_GTK_H

#include <gtk/gtk.h>

// Declarations-----------------------------------------------------------------
int load_from_glade(GtkWidget ** window,
                    GtkBuilder ** builder);

void connect_signals(GtkBuilder * builder,
                     GtkWidget * window);

void button_pressed(GtkWidget * widget,
                    GdkEvent * event,
                    gpointer data);

gboolean window_delete(GtkWidget * widget,
                       GdkEvent * event,
                       gpointer data);
// Declarations-----------------------------------------------------------------

#endif
